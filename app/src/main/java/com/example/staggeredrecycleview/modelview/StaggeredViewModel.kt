package com.example.staggeredrecycleview.modelview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.staggeredrecycleview.model.TypesListRepo

class StaggeredViewModel: ViewModel() {
    private var _typeList = MutableLiveData<List<Any>>()
    val typeList: LiveData<List<Any>> get() = _typeList

    fun getTypeList(id: Int){
        _typeList.value = TypesListRepo.getTypeList(id)
    }
}