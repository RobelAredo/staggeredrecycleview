package com.example.staggeredrecycleview.model.remote

interface TypesListService {
    fun typesList (id: Int): List<Any>
}