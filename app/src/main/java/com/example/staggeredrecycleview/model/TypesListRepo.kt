package com.example.staggeredrecycleview.model

import com.example.staggeredrecycleview.model.remote.TypesListService
import kotlin.random.Random

object TypesListRepo {
    private val typesListService = object: TypesListService {
        val size = 10

        fun typeList(generator: ()->Any): List<Any>{
            return List(size){generator()}
        }

        val typesList = listOf(
            listOf("Bob!", "Dog🐶", "Apropos",
                "Highfalutin", "Tohubohu", "Desiderata",
                "Kludge", "Malaise", "Akrasia", "Flummoxed"),
            typeList { Random.nextInt() },
            typeList { Random.nextBoolean() },
            typeList { Random.nextDouble() },
            typeList { Random.nextInt().toChar() },
            typeList { List(Random.nextInt(10)){(Random.nextInt(26)+97).toChar()} },
            typeList { Random.nextBytes(10) },
            typeList { Random.nextFloat() },
            typeList { Random.nextLong() },
            typeList { Pair(Random.nextInt().toChar(), Random.nextBoolean()) },
            typeList { Triple(Random.nextInt().toChar(), Random.nextBoolean(), Random.nextInt()) },
            typeList { List(Random.nextInt(10)) { Random.nextInt(26) + 97 }.associateBy { it.toChar() } }
        )


        override fun typesList(id: Int): List<Any> {
            return typesList[id]
        }
    }

    fun getTypeList(id: Int): List<Any> {
        return typesListService.typesList(id)
    }
}