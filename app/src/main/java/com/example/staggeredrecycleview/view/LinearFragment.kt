package com.example.staggeredrecycleview.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.staggeredrecycleview.databinding.FragmentLinearBinding
import com.example.staggeredrecycleview.modelview.StaggeredViewModel

class LinearFragment: Fragment() {
    private var _binding: FragmentLinearBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<LinearFragmentArgs>()
    private val staggeredViewModel by viewModels<StaggeredViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentLinearBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        staggeredViewModel.getTypeList(args.id)
        with(binding){
            staggeredViewModel.typeList.observe(viewLifecycleOwner){ typeList ->
                rvLinearList.adapter = ListAdapter().apply { addTypes(typeList.map{"$it"}) }
            }
            btnBack.setOnClickListener { findNavController().navigate(LinearFragmentDirections.actionLinearFragmentToStaggeredFragment()) }
        }
    }
}