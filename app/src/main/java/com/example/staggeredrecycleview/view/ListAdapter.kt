package com.example.staggeredrecycleview.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.staggeredrecycleview.databinding.ItemTypeBinding
import kotlin.random.Random

class ListAdapter(private val navigate: ((Int)->Unit)? = null): RecyclerView.Adapter<ListAdapter.StaggeredViewHolder>() {

    private var typesList = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StaggeredViewHolder = StaggeredViewHolder(
        ItemTypeBinding.inflate(LayoutInflater.from(parent.context), parent, false)).apply {
            binding.tvType.setOnClickListener{
                if(navigate != null) navigate!!(adapterPosition)
            }
    }

    override fun onBindViewHolder(holder: StaggeredViewHolder, position: Int) {
        holder.loadTypes(navigate, typesList[position])
    }

    override fun getItemCount(): Int {
        return typesList.size
    }

    fun addTypes(types: List<String>){
        typesList = types.toMutableList()
        notifyDataSetChanged()
    }

    class StaggeredViewHolder (val binding: ItemTypeBinding): RecyclerView.ViewHolder(binding.root) {
        fun loadTypes (navigate: ((Int) -> Unit)?, type: String) {
            binding.tvType.text = type
            if (navigate != null) {
                val textSize = (Random.nextInt(6) + 3) * 10f
                binding.tvType.textSize = textSize
                binding.tvType.textSize = textSize
            }
        }
    }
}