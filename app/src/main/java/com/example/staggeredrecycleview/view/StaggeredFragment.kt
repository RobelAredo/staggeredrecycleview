package com.example.staggeredrecycleview.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.staggeredrecycleview.databinding.FragmentStaggeredBinding

class StaggeredFragment: Fragment() {
    private var _binding: FragmentStaggeredBinding? = null
    private val binding get() = _binding!!

    private val typeList = listOf("String", "Int", "Boolean", "Double", "Char", "List",
    "Bytes", "Float", "Long", "Pair", "Triple", "Map")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentStaggeredBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding){
            rvStaggeredList.adapter = ListAdapter(::navigate).apply { addTypes(typeList) }
        }
    }

    fun navigate(id: Int){
        findNavController().navigate(StaggeredFragmentDirections.actionStaggeredFragmentToLinearFragment(id))
    }
}

